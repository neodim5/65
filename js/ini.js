
// import {user} from './car.js'

// user()

if (document.querySelector('.speaker-mesh')) {
    document.querySelector('.speaker-mesh').innerHTML = `
    <div class="col-md-4 speaker">
      <img src="images/speaker/Чернявский.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
       <h3>Чернявский Александр Михайлович </h3>
       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/HM.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Хироми Мацубара</h3>
        <span class="position">Okayama Medical Center, Окаяма, Япония</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Романов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Романов Александр Борисович </h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Ломиворотов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Ломиворотов Владимир Владимирович </h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Богачев.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Богачев-Прокофьев Александр Владимирович </h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Красильников.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Красильников Сергей Эдуардович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Крестьянинов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Крестьянинов Олег Викторович </h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Семин.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Семин Павел Александрович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Таранов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Таранов Павел Александрович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Альсов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Альсов Сергей Анатольевич</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Кузнецова.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Кузнецова Таисия Валентиновна</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Волкова.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Волкова Ирина Ивановна</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Горицкий.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Горицкий Антон Михайлович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Шабанов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Шабанов Виталий Викторович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Щекодько.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Щекодько Оксана Викторовна</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Сирота.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Сирота Дмитрий Андреевич</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>

    <div class="col-md-4 speaker">
      <img src="images/speaker/Остальцев.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Остальцев Илья Александрович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>


    <div class="col-md-4 speaker">
      <img src="images/speaker/Чернов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
      <div class="text text-center pt-3">
        <h3>Чернов Сергей Владимирович</h3>
        <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
      </div>
    </div>
`
}



if (document.getElementById('speaker-carousel')) {
//     document.getElementById('speaker-carousel').innerHTML = `
// <div class="container">
//       <div class="row justify-content-center mb-5 pb-3">
//     <div class="col-md-7 text-center heading-section ftco-animate">
//       <h2 class="mb-4"><span></span> Лекторы</h2>
//     </div>
//   </div>
//   <div class="row">
//       <div class="col-md-12 ftco-animate">
//           <div class="carousel-testimony owl-carousel">
//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Чернявский.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Чернявский Александр Михайлович </h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Романов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Романов Александр Борисович </h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Ломиворотов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Ломиворотов Владимир Николаевич </h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Богачев.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Богачев-Прокофьев Александр Владимирович </h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Крестьянинов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Крестьянинов Олег Викторович </h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Семин.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Семин Павел Александрович</h3>
//                           <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Таранов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Таранов Павел Александрович</h3>
//                                       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Альсов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Альсов Сергей Анатольевич</h3>
//                                       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Кузнецова.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Кузнецова Таисия Валентиновна</h3>
//                                       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Волкова.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Волкова Ирина Ивановна</h3>
//                                       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Горицкий.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Горицкий Антон Михайлович</h3>
//                                       <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>


//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Шабанов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Шабанов Виталий Викторович</h3>
//                                                   <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Щекодько.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Щекодько Оксана Викторовна</h3>
//                                                   <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Сирота.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Сирота Дмитрий Андреевич</h3>
//                                                   <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Остальцев.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Остальцев Илья Александрович</h3>
//                                                   <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>

//               <div class="item">
//                   <div class="speaker">
//                       <img src="images/speaker/Чернов.jpg" class="img-fluid" alt="Colorlib HTML5 Template">
//                       <div class="text text-center pt-3">
//                           <h3>Чернов Сергей Владимирович</h3>
//                                                   <span class="position">ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</span>
//                                   </div>
//                   </div>
//               </div>


//           </div>
//       </div>
//   </div>
//   </div>
// `

}



if (document.querySelector('#footer-el')) {
    document.querySelector('#footer-el').innerHTML = `<div class="container">
    <div class="row mb-5">
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">XI Научные чтения</h2>
          <p>Научно-практическая конференция «XI Научные чтения, посвященные памяти академика Е.Н. Мешалкина. Коморбидный пациент – трансформация взглядов и новые горизонты в ХХI веке».</p>
    
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-5">
          <h2 class="ftco-heading-2">О конференции</h2>
          <ul class="list-unstyled">
            <li><a href="about.html" class="py-2 d-block">О конференции</a></li>
            <li><a href="schedule.html" class="py-2 d-block">Программа</a></li>
            <li><a href="speakers.html" class="py-2 d-block">Лекторы</a></li>
            <li><a href="blog.html" class="py-2 d-block">Требования</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
         <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2"></h2>
          <ul class="list-unstyled">
            <li><a href="registration.html" class="py-2 d-block">Регистрация</a></li>
            <li><a href="partners.html" class="py-2 d-block">Участие организаций</a></li>
            <li><a href="contact.html" class="py-2 d-block">Information in English</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Остались вопросы?</h2>
            <div class="block-23 mb-3">
              <ul>
                <li><span class="icon icon-map-marker"></span><span class="text">г. Новосибирск, ул. Речкуновская, 15</span></li>
                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+7 (913) 470-47-83</span></a></li>
                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">conf@meshalkin.ru</span></a></li>
              </ul>
            </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
    
        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> <p>ФГБУ «НМИЦ им. ак. Е.Н. Мешалкина» Минздрава России, Новосибирск</p>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
    </div>
    </div>` 
}


if (document.getElementById('text-in-english')) {
  document.getElementById('text-in-english').innerHTML = `          <div class="col-md-12 mb-4">
  <!-- <h2 class="h3">Contact Information</h2>
  <span class="subheading">О конференции </span> -->
  <h2 class="mb-4"><span></span>  </h2>


<p> <b>Dear Colleagues, </b> </p>
<p> The XI Educational and Scientific Conference dedicated to the memory of Academician E.N. Meshalkin Patient with cardiovascular diseases and comorbidities – modern treatment insights and new horizons in XХI century takes place in Novosibirsk on June 16-18, 2022 to celebrate the 65th anniversary of the E.N. Meshalkin National Medical Research Center.
  Both online and onsite, the event will summon cardiologists, cardio-vascular surgeons, oncologists, neurosurgeons and anesthesiologists. 
  The conference's agenda will cover the whole spectrum of clinical research carried out at E.N. Meshalkin National Medical Research Center. Together we will discuss the scientific and clinical issues of surgery and consider the results of the most advanced research. The agenda is built around such topical problems as CAD and CTPH treatment; innovative endovascular technologies to treat cardiovascular pathologies; diagnostics and surgery of congenital heart diseases; anesthesiology and arrythmia treatment; treatment of malignant tumors; intervascular surgery and extrapyramidal syndrome treatment. 
  I cordially invite you all to join us the Novosibirsk, where we can fruitfully share our experience and best practices to make the best use of them in our daily practices!
  </p>
<p> <b>Sincerely yours, A. M. Chernyavskiy
  
  </b> </p>
  <p><b>Director General, MD, PhD  </b></p>


<p> <a href="/"></a> Program 

  </p>

<p>            The conference will be streamed online at www.pruffme.com
</p>  
<p>Information for Lecturers (requierements) </p>


<ul>
<li> <a href="images/Information for Lecturers.pdf" target="_blank">Please see the requirements for the provided materials </a> </li>
<li> <a href="images/The XI Educational and Scientific Conference PPT.ppt" target="_blank">We suggest using the conference template for your presentations </a> </li>
<li> We suggest using the conference template for  Е-poster </li>
<li> <a href="images/backstage_template.jpg" target="_blank"> We suggest using the conference backstage template for pre-recorded presentation </a> </li>
</ul>

<p> <a href=" https://forms.yandex.ru/cloud/624e8d469f0217775796b8bd/">REGISTRATION</a> </p>`
}

if (document.getElementById('org-comittee')) {
  document.getElementById('org-comittee').innerHTML = `        <div class="row d-flex mb-5 contact-info">
  <div class="col-md-12 mb-4">
  </div>
  <div class="w-200" style="width: 200px;">
  </div>
  <div class="col-md-5">

    <h3><p><span>Organizing Committee:</span> </p></h3>
    <p>“E. Meshalkin National medical research center”
      of the Ministry of Health of the Russian Federation
      </p>
    <p><span>Address:</span> Rechkunovskaya str., 15, Novosibirsk, 630055</p>
    <p><span>Site:</span> <a href="https://meshalkin.ru/" target="_blank">www.meshalkin.ru</a></p>
    <p><span>Tel:</span> <a href="tel://+7 (913) 470-47-83">+7 (913) 470-47-83</a></p>
    <p><span>Email:</span> <a href="mailto:conf@meshalkin.ru ">conf@meshalkin.ru </a></p>
    
  </div>

</div>

</div>
  `
}


console.log(new Date().getMonth())
